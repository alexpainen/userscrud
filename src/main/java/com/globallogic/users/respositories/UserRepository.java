package com.globallogic.users.respositories;

import com.globallogic.users.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserModel, UUID> {
    public abstract Optional<UserModel> findById(UUID uuid);
    public abstract ArrayList<UserModel> findByName(String name);
}