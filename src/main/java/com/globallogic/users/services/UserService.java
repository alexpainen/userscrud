package com.globallogic.users.services;

import com.globallogic.users.models.UserModel;
import com.globallogic.users.respositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    Pattern emailPattern = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");
    Pattern passwordPattern = Pattern.compile("(?=.*[0-9]{2,})(?=.*[a-z])(?=.*[A-Z])");

    public ResponseEntity getUsers() {
        try {
            log.debug("looking for all user ");
            List<UserModel> users = userRepository.findAll();
            if (users.isEmpty()) {
                log.debug("users not found");
                return ResponseEntity.notFound().build();
            } else {
                log.debug("get users");
                return ResponseEntity.ok(users);
            }
        } catch (Exception e) {
            log.error("mensaje : " + e.getMessage());
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }

    public ResponseEntity getUser(UUID uuid) {
        try {
            log.debug("looking for user id : " + uuid);
            Optional<UserModel> user = userRepository.findById(uuid);
            if (user.isPresent()) {
                log.debug("get user");
                return ResponseEntity.ok(user.get());
            } else {
                log.debug("user not found, id : " + uuid);
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            log.error("message : " + e.getMessage());
            return ResponseEntity.internalServerError().body("mensaje : " + e.getMessage());
        }
    }

    public ResponseEntity getUser(String name) {
        try {
            log.debug("searching user by name : " + name);
            List<UserModel> users = userRepository.findByName(name);
            if (users.isEmpty()) {
                log.debug("users not found");
                return ResponseEntity.notFound().build();
            } else {
                log.debug(users.size() + " users found for the name : " + name);
                return ResponseEntity.ok(users);
            }
        } catch (Exception e) {
            log.error("message : " + e.getMessage());
            return ResponseEntity.internalServerError().body("mensaje : " + e.getMessage());
        }
    }

    public ResponseEntity setUser(UserModel user) {
        try {
            log.debug("creating user");
            Matcher emailMatcher = emailPattern.matcher(user.getEmail());
            Matcher passwordMatcher = passwordPattern.matcher(user.getPassword());

            if (!emailMatcher.find()) {
                log.debug("validating email format");
                return ResponseEntity.badRequest().body("mensaje : invalid email format");
            } else if (!passwordMatcher.find()) {
                log.debug("validating password format");
                return ResponseEntity.badRequest().body("mensaje : invalid password format");
            } else {
                URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId()).toUri();
                log.debug("uri : " + uri.toString());
                return ResponseEntity.created(uri).body(userRepository.save(user));
            }
        } catch (DataIntegrityViolationException dataIntegrityViolationException) {
            log.error("message : " + dataIntegrityViolationException.getMessage());
            return ResponseEntity.internalServerError().body(new String("mensaje : El correo ya registrado"));
        } catch (Exception e) {
            log.error("message : " + e.getMessage());
            return ResponseEntity.internalServerError().body(new String("mensaje : " + e.getMessage()));
        }
    }

    public ResponseEntity updateUser(UserModel user) {
        try {
            if (userRepository.existsById(user.getId())) {
                log.debug("validating existing user");
                Matcher emailMatcher = emailPattern.matcher(user.getEmail());
                Matcher passwordMatcher = passwordPattern.matcher(user.getPassword());
                if (!emailMatcher.find()) {
                    log.debug("validating email format");
                    return ResponseEntity.badRequest().body("mensaje : invalid email format");
                } else if (!passwordMatcher.find()) {
                    log.debug("validating password format");
                    return ResponseEntity.badRequest().body("mensaje : invalid password format");
                } else {
                    log.debug("updating user");
                    user.setModified(new Date());
                    return ResponseEntity.ok(userRepository.save(user));
                }
            } else {
                log.debug("user not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("mensaje : user not found");
            }
        } catch (DataIntegrityViolationException dataIntegrityViolationException) {
            log.error("message : " + dataIntegrityViolationException.getMessage());
            return ResponseEntity.internalServerError().body(new String("mensaje : El correo ya registrado"));
        } catch (Exception e) {
            log.error("message : " + e.getMessage());
            return ResponseEntity.internalServerError().body("mensaje : " + e.getMessage());
        }
    }

    public ResponseEntity deleteUser(UUID uuid) {
        try {
            log.debug("validating existing user");
            if (userRepository.findById(uuid).isPresent()) {
                log.info("deleting user");
                userRepository.deleteById(uuid);
                return ResponseEntity.noContent().build();
            } else {
                log.debug("user not found");
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            log.error("message : " + e.getMessage());
            return ResponseEntity.internalServerError().body("mensaje : " + e.getMessage());
        }
    }
}