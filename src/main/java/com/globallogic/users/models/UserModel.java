package com.globallogic.users.models;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Type;
import org.springframework.web.bind.annotation.PathVariable;

import javax.persistence.*;
import java.text.Format;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
@Data
@Table(name = "users")
public class UserModel {
    @Id
    @Column(name = "user_id")
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();
    private String name;
    @Column (unique = true)
    private String email;
    private String password;
    private Date crated = new Date();
    private Date modified ;
    private Date lastLogin = new Date();
    private String token;
    private boolean isActive;
    @OneToMany(cascade = CascadeType.ALL)
    private List<PhoneModel> phones;
}