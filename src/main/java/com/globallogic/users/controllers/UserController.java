package com.globallogic.users.controllers;

import com.globallogic.users.models.UserModel;
import com.globallogic.users.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping()
    public ResponseEntity<List<UserModel>> getUsers() {
        return userService.getUsers();
    }

    @GetMapping(path = "/{uuid}")
    public ResponseEntity<UserModel> getUserById(@PathVariable UUID uuid) {
        return userService.getUser(uuid);
    }

    @GetMapping(path = "/{name}")
    public ResponseEntity<List<UserModel>> getUserByName(@PathVariable String name) {
        return userService.getUser(name);
    }

    @PostMapping
    public ResponseEntity<UserModel> setUser(@RequestBody UserModel user) {
        return userService.setUser(user);
    }

    @PutMapping
    public ResponseEntity<UserModel> putUser(@RequestBody UserModel user) {
        return userService.updateUser(user);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<UserModel> delUserById(@PathVariable UUID id) {
        return userService.deleteUser(id);
    }
}