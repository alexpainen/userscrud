# UsersCRUD
## Descripción
Interfas RESTful para administracion de usuarios con base de datos en memoria
## Instalación
Se debe descargar el proyecto y luego ejecutar el comando
    <code>gradlew bootRun</code>
Para poder crear un usuario se debe usar un JSON con el siguiente formato
    <pre><code>
    {
    "name": "Juan Rodriguez",
    "email": "juan@rodriguez.org",
    "password": "HHunter22",
    "phones": [
        {
            "number": 1234567,
            "cityCode": 1,
            "countryCode": 57
        },
        {
            "number": 7654321,
            "cityCode": 1,
            "countryCode": 57
        },
        {
            "number": 7856949,
            "cityCode": 1,
            "countryCode": 57
        }
    ]
}
    </code></pre>
el Status dependera de la respuesta del servicio
